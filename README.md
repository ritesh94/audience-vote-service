Frontend: /reveal.js-3.6.0
Backend: /vote-counter-service

* Install
```
sudo apt install npm
cd reveal.js-3.6.0
npm install
```

* Start reveal.js server
```
cd reveal.js-3.6.0
npm start -- --port=8001
```

* Start vote counter service
```
cd vote-counter-service
python3 main.py --port 5000
```

* Voting URL
```
http://127.0.0.1:5000/send-vote?vote=<slide direction>
<slide direction> can equal 'right' or 'down'
```

* View the votes:
```
http://127.0.0.1:8000/demo.html#/2
```
