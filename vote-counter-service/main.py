from flask import Flask, Response, redirect, url_for, request, session, abort
from flask_cors import CORS
import requests
import json
import pickle
from threading import Timer
import datetime
import codecs
import collections
from subprocess import call
from optparse import OptionParser

parser = OptionParser()
parser.add_option("-p", "--port", dest="port_number", help="port number for this service", type=int)

(options, args) = parser.parse_args()

if options.port_number is None:
    options.port_number = 5000
    print('port number not provided. starting the service on the default port: 5000')

app = Flask(__name__)
CORS(app)

right = 0
down = 0

visitor_votes = {}

@app.route('/send-vote', methods=['GET'])
def vote():
	visitor = request.remote_addr
	if visitor in visitor_votes:
		return '<html><body><center><h1>Already voted.</h1><p style="font-size: 24px;">You have already voted for <b>{}</b> before.</p><br /><p style="font-size: 24px;">Thank you.</p></center></body></html>'.format(visitor_votes[visitor])
	global right
	global down
	vote = request.args.get('vote')
	if vote == "right":
		right += 1
	else:
		down += 1
	visitor_votes[visitor] = vote
	return '<html><body><center><h1>Thank you!</h1><p style="font-size: 24px;">Your vote to go <b>{}</b> in the presentation has been submitted.</p></center></body></html>'.format(vote)


@app.route('/get-vote', methods=['GET'])
def get_vote():
	global right
	global down
	return json.dumps({"right": right, "down": down})

if __name__ == '__main__':
    print('Flask Web Service started...')
    app.run(host='0.0.0.0', port=options.port_number, threaded=True)